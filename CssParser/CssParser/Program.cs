﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ColorMine.ColorSpaces;
using ExCSS;

namespace CssParser
{
    // https://en.wikipedia.org/wiki/Web_colors
    public class Program
    {
        static void Main(string[] args)
        {
            var styleFile = @"bootstrap.css";
            var styleText = File.ReadAllText(styleFile);
            var parser = new Parser();
            var css = parser.Parse(styleText);

            // Delete any non style rules, they won't have colors
            var allRules = css.Rules.Where(r => r.RuleType != RuleType.Style).ToList();
            allRules.Each(r => css.RemoveRule(css.Rules.IndexOf(r)));

            var styleRules = css.StyleRules.ToList();

            // Remove all rules and declarations that aren't related to color
            styleRules.Each(r =>
            {
                var decsWithColor = r.Declarations.Where(d => d.Term.ContainsAColorProperty()).ToList();
                var otherDecs = r.Declarations.Where(d => !d.Term.ContainsAColorProperty()).ToList();

                // If the rule had no color declorations remove the whole rule
                if (decsWithColor.Count == 0)
                {
                    css.RemoveRule(css.StyleRules.IndexOf(r));
                }
                // Remove any non color related delorations rom the rule
                r.Declarations.Where(d => !d.Term.ContainsAColorProperty()).ToList().Each(d => r.Declarations.Remove(d));

            });

            var outFile = $"{Path.GetFileNameWithoutExtension(styleFile)}_out{Path.GetExtension(styleFile)}";
            File.WriteAllText(outFile, css.ToString(true, 0).SwitchColors());
            System.Diagnostics.Process.Start(outFile);
            Console.ReadKey();

        }
    }

    public static class Helpers
    {
        public static void Each<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }

        public static void Print(this object o)
        {
            Console.WriteLine(o.ToString());
        }

        public static bool ContainsAColorProperty(this Term cssTerm)
        {
            return //Regex.IsMatch(cssTerm.ToString(), Assets.COLORPSPATTERN, RegexOptions.IgnoreCase)
                 Regex.IsMatch(cssTerm.ToString(), Assets.HEXPATTERN, RegexOptions.IgnoreCase)
                || Regex.IsMatch(cssTerm.ToString(), Assets.RGBPATTERN, RegexOptions.IgnoreCase)
                || Regex.IsMatch(cssTerm.ToString(), Assets.HSLPATTERN, RegexOptions.IgnoreCase);
        }

        public static string SwitchColors(this string cssString)
        {
            var result = Regex.Replace(cssString, Assets.COLORPSPATTERN, HtmlColorMatchEvaluater, RegexOptions.IgnoreCase);
            result = Regex.Replace(result, Assets.HEXPATTERN, HexMatchEvaluater, RegexOptions.IgnoreCase);
            result = Regex.Replace(result, Assets.RGBPATTERN, RgbMatchEvaluater, RegexOptions.IgnoreCase);
            result = Regex.Replace(result, Assets.HSLPATTERN, HslMatchEvaluater, RegexOptions.IgnoreCase);
            return result;

        }

        public static string HtmlColorMatchEvaluater(Match match)
        {
            var val = match.Value;
            var c = ColorTranslator.FromHtml(match.Value);
            c = c.GetNearestColor();
            var hex = ColorTranslator.ToHtml(c).ToLower();
            return hex;
        }

        public static string HexMatchEvaluater(Match match)
        {
            var val = match.Value;
            var c = ColorTranslator.FromHtml(match.Value);
            c = c.GetNearestColor();
            var hex = ColorTranslator.ToHtml(c).ToLower();
            return hex;
        }

        public static string RgbMatchEvaluater(Match match)
        {
            var r = match.Groups["r"].Value.GetRgbValue();
            var g = match.Groups["g"].Value.GetRgbValue();
            var b = match.Groups["b"].Value.GetRgbValue();
            var a = match.Groups["a"].Value;

            var c = Color.FromArgb(r, g, b);
            c = c.GetNearestColor();

            if (!string.IsNullOrEmpty(a))
            {
                var alpha = Convert.ToInt16(double.Parse(a) * 255);
                c = Color.FromArgb(alpha, c);
                return $"rgba({c.R}, {c.G}, {c.B},{a})";
            }

            var hex = ColorTranslator.ToHtml(c);
            return hex;
        }

        public static string HslMatchEvaluater(Match match)
        {
            var h = match.Groups["h"].Value.GetRgbValue();
            var s = match.Groups["s"].Value.GetRgbValue();
            var l = match.Groups["l"].Value.GetRgbValue();
            var a = match.Groups["a"].Value;

            Hsl hsl = new Hsl() { H = h, S = s, L = l };
            var rgb = hsl.ToRgb();
            var c = Color.FromArgb(Convert.ToInt32(rgb.R), Convert.ToInt32(rgb.G), Convert.ToInt32(rgb.B));
            c = c.GetNearestColor();

            if (!string.IsNullOrEmpty(a))
            {
                var alpha = Convert.ToInt16(double.Parse(a) * 255);
                c = Color.FromArgb(alpha, c);
                return $"hsla({c.GetHue()}, { Math.Round(c.GetSaturation() * 100)}%, {Math.Round(c.GetBrightness() * 100)}%,{a})";
            }

            var hex = ColorTranslator.ToHtml(c);
            return hex;
        }

        public static int GetRgbValue(this string val)
        {
            if (val.Contains("%"))
            {
                val = val.Replace("%", string.Empty);
                double percent = double.Parse(val);
                return Convert.ToInt32(255 * percent / 100);
            }

            var value = int.Parse(val);
            return value < 1 ? 255 * value : value;
        }

        public static Color GetNearestColor(this Color c)
        {
            return ColorTranslator.FromHtml("yellow");
        }

    }
}
