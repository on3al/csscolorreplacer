﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CssParser
{
    public static class Assets
    {
        public const string HEXPATTERN = @"(#[0-9a-fA-F]{6}|#[0-9a-fA-F]{3})";
        public const string RGBPATTERN = @"rgba*\s*\((?<r>\d*\.*\d*\s*%*)\s*,\s*(?<g>\d*\.*\d*%*)\s*,\s*(?<b>\d*\.*\d%*)\s*,*\s*(?<a>\d*\.*\d*)\)";
        public const string HSLPATTERN = @"hsla*\s*\((?<h>\d*\.*\d*)\s*,\s*(?<s>\d*\.*\d*)%*\s*,\s*(?<l>\d*\.*\d*)%*\s*,*\s*(?<a>\d*\.*\d+)*\s*\)";
        public static readonly string COLORPSPATTERN;

        static Assets()
        {
            HtmlColors = _htmlColors.Select(c => c.ToLower()).ToList();

            var sb = new StringBuilder();
            sb.Append(@"(");
            HtmlColors.Each(c => sb.Append($@"[^\w-]{c}[^\w-]|"));

            COLORPSPATTERN = $"{sb.ToString().TrimEnd('|')})";
        }


        public static List<string> CssPropertiesWithColor = new List<string>
        {
            "color",
            "background",
            "background-color",
            "border",
            "border-bottom",
            "border-top",
            "border-left",
            "border-right",
            "border-bottom-color",
            "border-top-color",
            "border-left-color",
            "border-right-color",
            "text-decoration",
            "text-decoration-color",
            "text-shadow",
            "box-shadow",
            "outline",
            "outline-color",
            "column-rule",
            "column-rule-color"
        };

        public static readonly List<string> HtmlColors;

        private static readonly List<string> _htmlColors = new List<string>()
        {
            "AliceBlue",
            "AntiqueWhite",
            "Aqua",
            "Aquamarine",
            "Azure",
            "Beige",
            "Bisque",
            "Black",
            "BlanchedAlmond",
            "Blue",
            "BlueViolet",
            "Brown",
            "BurlyWood",
            "CadetBlue",
            "Chartreuse",
            "Chocolate",
            "Coral",
            "CornflowerBlue",
            "Cornsilk",
            "Crimson",
            "Cyan",
            "DarkBlue",
            "DarkCyan",
            "DarkGoldenRod",
            "DarkGray",
            "DarkGrey",
            "DarkGreen",
            "DarkKhaki",
            "DarkMagenta",
            "DarkOliveGreen",
            "DarkOrange",
            "DarkOrchid",
            "DarkRed",
            "DarkSalmon",
            "DarkSeaGreen",
            "DarkSlateBlue",
            "DarkSlateGray",
            "DarkSlateGrey",
            "DarkTurquoise",
            "DarkViolet",
            "DeepPink",
            "DeepSkyBlue",
            "DimGray",
            "DimGrey",
            "DodgerBlue",
            "FireBrick",
            "FloralWhite",
            "ForestGreen",
            "Fuchsia",
            "Gainsboro",
            "GhostWhite",
            "Gold",
            "GoldenRod",
            "Gray",
            "Grey",
            "Green",
            "GreenYellow",
            "HoneyDew",
            "HotPink",
            "IndianRed",
            "Indigo",
            "Ivory",
            "Khaki",
            "Lavender",
            "LavenderBlush",
            "LawnGreen",
            "LemonChiffon",
            "LightBlue",
            "LightCoral",
            "LightCyan",
            "LightGoldenRodYellow",
            "LightGray",
            "LightGrey",
            "LightGreen",
            "LightPink",
            "LightSalmon",
            "LightSeaGreen",
            "LightSkyBlue",
            "LightSlateGray",
            "LightSlateGrey",
            "LightSteelBlue",
            "LightYellow",
            "Lime",
            "LimeGreen",
            "Linen",
            "Magenta",
            "Maroon",
            "MediumAquaMarine",
            "MediumBlue",
            "MediumOrchid",
            "MediumPurple",
            "MediumSeaGreen",
            "MediumSlateBlue",
            "MediumSpringGreen",
            "MediumTurquoise",
            "MediumVioletRed",
            "MidnightBlue",
            "MintCream",
            "MistyRose",
            "Moccasin",
            "NavajoWhite",
            "Navy",
            "OldLace",
            "Olive",
            "OliveDrab",
            "Orange",
            "OrangeRed",
            "Orchid",
            "PaleGoldenRod",
            "PaleGreen",
            "PaleTurquoise",
            "PaleVioletRed",
            "PapayaWhip",
            "PeachPuff",
            "Peru",
            "Pink",
            "Plum",
            "PowderBlue",
            "Purple",
            "RebeccaPurple",
            "Red",
            "RosyBrown",
            "RoyalBlue",
            "SaddleBrown",
            "Salmon",
            "SandyBrown",
            "SeaGreen",
            "SeaShell",
            "Sienna",
            "Silver",
            "SkyBlue",
            "SlateBlue",
            "SlateGray",
            "SlateGrey",
            "Snow",
            "SpringGreen",
            "SteelBlue",
            "Tan",
            "Teal",
            "Thistle",
            "Tomato",
            "Turquoise",
            "Violet",
            "Wheat",
            "White",
            "WhiteSmoke",
            "Yellow",
            "YellowGreen"
        };
    }
}
